﻿using Microsoft.EntityFrameworkCore;
using ADMassignment.Models;

namespace ADMassignment.Data
{
    public class AdmProduktContext : DbContext
    {
        public AdmProduktContext (DbContextOptions<AdmProduktContext> options) : base(options)
        {

        }

        public DbSet<Produkt> Produkt { get; set; }
    }
}
