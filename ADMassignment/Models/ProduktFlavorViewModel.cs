﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ADMassignment.Models
{
    public class ProduktFlavorViewModel
    {
        public List<Produkt> Produkts { get; set; }
        public SelectList Flavors { get; set; }
        public string ProduktFlavor { get; set; }
        public string SearchString { get; set; }
    }
}
