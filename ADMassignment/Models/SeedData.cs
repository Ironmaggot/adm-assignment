﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ADMassignment.Data;

namespace ADMassignment.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new AdmProduktContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<
                        AdmProduktContext>>()))
            {
                //Look for any products
                if (context.Produkt.Any())
                {
                    return; // DB has been seeded
                }

                context.AddRange(
                    new Produkt
                    {
                        Title = "Pie",
                        BestBeforeDate = DateTime.Parse("2020-4-19"),
                        Flavor = "Blueberry",
                        Price = 3.14M,
                        Rating = "A"
                    },

                    new Produkt
                    {
                        Title = "Cake",
                        BestBeforeDate = DateTime.Parse("2020-4-17"),
                        Flavor = "Banana",
                        Price = 20.0M,
                        Rating = "B"
                    },

                    new Produkt
                    {
                        Title = "Muffin",
                        BestBeforeDate = DateTime.Parse("2020-4-15"),
                        Flavor = "Blueberry",
                        Price = 2.50M,
                        Rating = "C"
                    },

                    new Produkt
                    {
                        Title = "Blin",
                        BestBeforeDate = DateTime.Parse("2020-4-13"),
                        Flavor = "Rasberry",
                        Price = 1.20M,
                        Rating = "D"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
